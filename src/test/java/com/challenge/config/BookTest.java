package com.challenge.config;

import com.challenge.dto.BookBuilderExample;
import com.challenge.dto.BookDTO;
import lombok.Cleanup;
import org.junit.Test;

import java.io.InputStream;

import static org.junit.Assert.assertEquals;

public class BookTest {

    @Test
    public void createBookTest(){
        BookDTO bookDTO = new BookDTO();
        bookDTO.setDescription("TestDescription");
        bookDTO.setSubtitle("TestSubtitle");
        BookDTO bookDTO2 = new BookDTO();

        bookDTO2.setDescription("TestDescription");
        bookDTO2.setSubtitle("TestSubtitle");

        if(bookDTO.equals(bookDTO2)){
            System.out.println("Equals");
        }

        System.out.println(bookDTO);
        assertEquals("TestDescription", bookDTO.getDescription());
    }

    @Test
    public void bookBuilderTest(){
        BookBuilderExample bookBuilderExample = BookBuilderExample.builder()
                .title("Cien años de soledad")
                .subtitle("Los inicios de macondo")
                .pages(30)
                .build();

        System.out.println(bookBuilderExample);

        assertEquals("Cien años de soledad", bookBuilderExample.getTitle());
    }

}
