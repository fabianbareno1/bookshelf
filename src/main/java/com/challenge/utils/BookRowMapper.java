package com.challenge.utils;

import com.challenge.dto.BookDTO;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class BookRowMapper implements RowMapper {

    @Override
    public BookDTO mapRow(ResultSet resultSet, int i) throws SQLException {
        BookDTO bookDTO = new BookDTO();
        bookDTO.setIsbn(resultSet.getString("isbn"));
        bookDTO.setTitle(resultSet.getString("title"));
        bookDTO.setSubtitle(resultSet.getString("subtitle"));
        bookDTO.setPublisher(resultSet.getString("publisher"));
        bookDTO.setPages(resultSet.getInt("pages"));
        bookDTO.setDescription(resultSet.getString("description"));
        return bookDTO;
    }
}
