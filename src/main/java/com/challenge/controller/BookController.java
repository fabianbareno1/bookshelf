package com.challenge.controller;

import com.challenge.dto.BookDTO;
import com.challenge.service.BookService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class BookController {

    private final BookService bookService;

    @Autowired
    public BookController(BookService bookService) {
        this.bookService = bookService;
    }

    @RequestMapping(value = "/book/{title}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "", notes = "Get the book's information searching by title",
            produces = MediaType.APPLICATION_JSON_VALUE)
    public List<BookDTO> getBookByTitle(
            @PathVariable(value="title") String title
    ){
        return bookService.getBookByTitle(title);
    }

    @RequestMapping(value = "/books", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "", notes = "Get all the book's information",
            produces = MediaType.APPLICATION_JSON_VALUE)
    public List<BookDTO> getBooks(){
        return bookService.getBooks();
    }


}
