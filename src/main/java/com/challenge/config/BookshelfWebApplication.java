package com.challenge.config;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages="com.challenge")
public class BookshelfWebApplication {

	public static void main(String[] args) {
		 SpringApplication.run(BookshelfWebApplication.class, args);
	}
}
