package com.challenge.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.ToString;

import java.time.LocalDateTime;
import java.util.List;

@Getter @Builder @ToString
public class BookBuilderExample {
    private String isbn;
    private String title;
    private String subtitle;
    private List<String> authors;
    private LocalDateTime published;
    private String publisher;
    private Integer pages;
    private String description;
    private Boolean inStock;
}
