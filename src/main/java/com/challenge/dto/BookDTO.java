package com.challenge.dto;

import io.swagger.annotations.ApiModel;
import lombok.*;

import java.time.LocalDateTime;
import java.util.List;

@ApiModel(value="Book")
@Getter @Setter @EqualsAndHashCode @ToString @NoArgsConstructor
public class BookDTO {

    private String isbn;
    private String title;
    private String subtitle;
    private List<String> authors;
    private LocalDateTime published;
    private String publisher;
    private Integer pages;
    private String description;
    private Boolean inStock;

}
