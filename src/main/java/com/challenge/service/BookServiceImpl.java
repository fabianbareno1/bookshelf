package com.challenge.service;


import com.challenge.dto.BookDTO;
import com.challenge.utils.BookRowMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class BookServiceImpl implements BookService{

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public BookServiceImpl(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Transactional(readOnly=true)
    public List<BookDTO> findAll() {
        return jdbcTemplate.query("select * from book",
                new BookRowMapper());
    }

    @Transactional(readOnly=true)
    public List<BookDTO> findByTitle(String title) {
        return jdbcTemplate.query(
                "select * from book where title LIKE ?",
                new Object[]{title}, new BookRowMapper());
    }

    @Override
    @Transactional
    public List<BookDTO> getBooks() {
        return findAll();
    }

    @Override
    @Transactional
    public List<BookDTO> getBookByTitle(String title) {
        return findByTitle(title);
    }

}
