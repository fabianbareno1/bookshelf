package com.challenge.service;

import com.challenge.dto.BookDTO;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface BookService {

    List<BookDTO> getBooks();

    List<BookDTO> getBookByTitle(String title);

}
